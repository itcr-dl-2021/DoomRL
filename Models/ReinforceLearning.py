from Agent import  *

def run_training(init_epoch=None):
    s3 = get_s3_client();
    game = create_default_game()
    doom_agent = DoomAgent()
    if init_epoch != None:
        download_file=settings.model_type
        r = requests.get(f"https://itcr-dl.s3.amazonaws.com/doomrl/training/{settings.model_type}/weights/{settings.model_type}-{init_epoch}.dat", allow_redirects=True)
        open(download_file, 'wb').write(r.content)
        doom_agent.load(f"{settings.model_type}.dat")
    else:
        init_epoch =0

    for epoch in range(init_epoch,settings.epochs):
        doom_agent.new_epoch()
        step_rewards = []
        episodes_end_health=[]
        episodes_end_ammo=[]
        episodes_rewards=[]
        epoch_action_count =0
        episode_action_count=0
        death_count = 0
        life_count = 0

        for episode in range(settings.episodes_per_epoch):
            game.new_episode()
            while not game.is_episode_finished():

                state  = game.get_state()
                vars = game.get_state().game_variables
                end_episode_health=vars[0]  # First is health
                end_episode_ammo=vars[1]    #Second is ammo
                state_ux = create_tensor(state)
                action = doom_agent.choose_action(state_ux)
                reward = game.make_action(settings.actions[action.item()])
                doom_agent.append_memory(state_ux,action,reward)
                step_rewards.append(reward)
                episode_action_count+=1
                epoch_action_count+=1
            print(game.get_total_reward())
            doom_agent.complete_episode(episode_action_count)
            episode_action_count=0
            episodes_rewards.append(game.get_total_reward())
            episodes_end_health.append( end_episode_health)
            episodes_end_ammo.append( end_episode_ammo)
            if game.is_player_dead():
                death_count+=1
            else:
                life_count+=1
        mean_step_reward=np.array(step_rewards).mean()
        mean_end_episode_health=np.array(episodes_end_health).mean()
        mean_end_episode_ammo=np.array(episodes_end_ammo).mean()
        mean_episode_reward=np.array(episodes_rewards).mean()
        mean_episode_action_count = epoch_action_count/settings.episodes_per_epoch
        epoch_status = {
            "mean_step_reward": mean_step_reward,
            "mean_episode_reward":mean_episode_reward,
            "mean_episode_action_count":mean_episode_action_count,
            "mean_end_episode_health":mean_end_episode_health,
            "mean_end_episode_ammo":mean_end_episode_ammo,
            "death_count":death_count,
            "life_count":life_count
        }

        batch_file = open("weights/status.dat", "wb")
        pickle.dump(epoch_status, batch_file)
        batch_file.close()
        s3.upload_file('weights/status.dat', 'itcr-dl', f"doomrl/training/{settings.model_type}/stats/{settings.model_type}-{epoch}.dat", ExtraArgs={'ACL': 'public-read'})

        print("")
        print(f"Epoch done {epoch}")
        print(f" Avg step rewards {mean_step_reward}")
        print(f" Avg episode rewards {mean_episode_reward}")
        print(f" Avg episode action count {mean_episode_action_count}")
        print(f" Avg episode end health {mean_end_episode_health}")
        print(f" Avg episode end ammo {mean_end_episode_ammo}")
        print(f" Lifes {life_count}")
        print(f" Deaths {death_count}")

        doom_agent.train()

        doom_agent.save(f"weights/{settings.model_type}-{epoch}.dat")
        #s3.upload_file(f'{settings.model_type}.dat', 'itcr-dl', f"doomrl/training/{settings.model_type}/weights/{settings.model_type}-{epoch}.dat", ExtraArgs={'ACL': 'public-read'})

