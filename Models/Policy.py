from BasicFunctions import *

class DoomAgentModel(nn.Module):
    def __init__(self):
        super(DoomAgentModel, self).__init__()
        self.l1_chls = 32
        self.l2_chls = self.l1_chls * 2  # 64
        self.l3_chls = self.l2_chls * 2  # 128
        self.l4_chls = self.l3_chls * 2  # 256
        self.l5_chls = self.l4_chls  # 256
        self.final_res = 8

        self.policy_layer_1 = self.l5_chls * self.final_res * self.final_res
        self.policy_layer_2 = self.l5_chls * self.final_res
        self.policy_layer_3 = self.l5_chls

        #  2x128x128 -> 32x64x64
        self.layer_1 = nn.Sequential(
            nn.Conv2d(3, self.l1_chls, kernel_size=3, padding=1),
            nn.PReLU(),
            nn.Conv2d(self.l1_chls, self.l1_chls, kernel_size=3, stride=2, padding=1),
            nn.PReLU(),
            nn.BatchNorm2d(self.l1_chls))

        # 32x64x64 -> 64x32x32
        self.layer_2 = nn.Sequential(
            nn.Conv2d(self.l1_chls, self.l2_chls, kernel_size=3, padding=1),
            nn.PReLU(),
            nn.Conv2d(self.l2_chls, self.l2_chls, kernel_size=3, stride=2, padding=1),
            nn.PReLU(),
            nn.BatchNorm2d(self.l2_chls))

        # 64x32x32 -> 128x32x32
        # Dilation to simulate fully connected
        self.layer_3 = nn.Sequential(
            nn.Conv2d(self.l2_chls, self.l3_chls, kernel_size=3, dilation=2, padding=2),
            nn.PReLU(),
            nn.Conv2d(self.l3_chls, self.l3_chls, kernel_size=3, dilation=2, padding=2),
            nn.PReLU(),
            nn.BatchNorm2d(self.l3_chls),
            nn.Conv2d(self.l3_chls, self.l3_chls, kernel_size=3, dilation=2, padding=2),
            nn.PReLU(),
            nn.Conv2d(self.l3_chls, self.l3_chls, kernel_size=3, dilation=2, padding=2),
            nn.PReLU(),
            nn.BatchNorm2d(self.l3_chls))

        # 128x32x32 -> 256x16x16
        self.layer_4 = nn.Sequential(
            nn.Conv2d(self.l3_chls, self.l4_chls, kernel_size=3, padding=1),
            nn.PReLU(),
            nn.Conv2d(self.l4_chls, self.l4_chls, kernel_size=3, stride=2, padding=1),
            nn.PReLU(),
            nn.BatchNorm2d(self.l4_chls))

        # 256x16x16 -> 256x8x8
        self.layer_5 = nn.Sequential(
            nn.Conv2d(self.l4_chls, self.l5_chls, kernel_size=3, padding=1),
            nn.PReLU(),
            nn.Conv2d(self.l5_chls, self.l5_chls, kernel_size=3, stride=2, padding=1),
            nn.PReLU(),
            nn.BatchNorm2d(self.l5_chls))

        self.policy = nn.Sequential(
            nn.Dropout(p=0.1),
            nn.Linear(self.policy_layer_1, self.policy_layer_2, bias=True),
            nn.PReLU(),
            nn.Linear(self.policy_layer_2, self.policy_layer_3, bias=True),
            nn.PReLU(),
            nn.Linear(self.policy_layer_3, settings.actions_size, bias=True),
            nn.PReLU()
        )

    def forward(self, input_1):
        conv_1 = self.layer_1(input_1)
        conv_2 = self.layer_2(conv_1)
        conv_3 = self.layer_3(conv_2)
        conv_4 = self.layer_4(conv_3)
        conv_5 = self.layer_5(conv_4)
        view = torch.flatten(conv_5, start_dim=1)
        policy = self.policy(view)
        return F.softmax(policy, dim=1)