from Dataset import *

class DoomAgent:
    def __init__(self):
        self.memory = []
        self.agent_model = None
        self.optimizer = None
        self.agent_model = DoomAgentModel()
        self.agent_model.cuda()
        self.optimizer = torch.optim.Adam(self.agent_model.parameters(), lr=0.000001)

    def load(self, checkpoint_file):
        checkpoint = torch.load(checkpoint_file)
        self.agent_model.load_state_dict(checkpoint['model'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.agent_model.eval()

    def save(self, checkpoint_file):
        torch.save({'model': self.agent_model.state_dict(),
                    'optimizer': self.optimizer.state_dict()}, checkpoint_file)

    def get_probs(self, state):
        torch.cuda.empty_cache()
        state = state.cuda()
        probs = self.agent_model.forward(state)
        probs = Categorical(probs)
        return probs

    def choose_action(self, state):
        with torch.no_grad():
            state = torch.stack((state,))
            probs = self.get_probs(state)
            action = probs.sample()
            return action

    def append_memory(self, state, episode_log_prob, action_reward):
        self.memory.append([state, episode_log_prob, action_reward])


    def complete_episode(self,action_count):
        """ Process discounted reward """
        new_rewards = []
        memlen = len(self.memory) #Only current episode
        for action_id in range(memlen-action_count,memlen):
            future_steps = memlen-action_id
            reward =0
            discount = 1
            for i in range(future_steps):
                reward += (discount*self.memory[action_id+i][2])
                discount *=  settings.discount_factor
            new_rewards.append(reward)
        # Update rewards
        counter = 0
        for action_id in range(memlen-action_count,memlen):
            self.memory[action_id+i][2]=new_rewards[counter]
            counter +=1


    def new_epoch(self):
        self.memory=[]


    def train_batch(self, state, action, rewards):
        probs = self.get_probs(state.cuda())
        log_probs = probs.log_prob(action)
        rewards = rewards.cuda()
        self.optimizer.zero_grad()
        rewards_applied = -log_probs * rewards
        loss = rewards_applied.sum()
        loss.backward()
        self.optimizer.step()

    def train(self):
        print("Training")
        train_loader = torch.utils.data.DataLoader(
            dataset=MemoryDataset(self.memory),
            batch_size=settings.batch_size,
            shuffle=True)
        for (state, action, rewards) in tqdm(train_loader, disable=True):
            self.train_batch(state, action, rewards)
