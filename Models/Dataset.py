from Policy import *

class MemoryDataset(Dataset):
    def __init__(self, memory):
        self.memory = memory

    def __len__(self):
        return len(self.memory)

    def __getitem__(self, idx):
        action = self.memory[idx]
        return action[0], action[1], action[2]
