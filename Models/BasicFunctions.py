from Imports import *
def create_action(pos, buttons_size):
    """ Creates one action """
    action = []
    for i in range(buttons_size):
        if i == pos:
            action.append(True)
        else:
            action.append(False)
    return action


def create_actions(buttons_size):
    """ Creates actions list """
    actions = []
    for pos in range(buttons_size):
        actions.append(create_action(pos, buttons_size))
    return actions


class GameSettings:
    def __init__(self):
        self.depth_buffer_enabled =True
        self.labels_buffer_enabled = True
        self.model_type = "final.improved.preprocessing"
        self.batch_size = 32
        self.doom_skill = 3
        self.episodes_per_epoch = 16
        self.discount_factor = 0.5
        self.screen_format = vzd.ScreenFormat.GRAY8
        self.buttons = [vzd.Button.MOVE_FORWARD,
                        vzd.Button.TURN_LEFT,
                        vzd.Button.TURN_RIGHT,
                        vzd.Button.ATTACK]
        self.actions = create_actions(len(self.buttons))
        self.actions_size = len(self.actions)
        self.resolution = 128, 128
        self.config_file_path = "scenarios/deadly_corridor.cfg"
        self.game_mode = vzd.Mode.ASYNC_PLAYER
        self.epochs = 32


settings = GameSettings()

def create_simple_game():
    print("Initializing doom...")
    game = vzd.DoomGame()
    game.load_config(settings.config_file_path)
    game.set_depth_buffer_enabled(settings.depth_buffer_enabled)
    game.set_labels_buffer_enabled(settings.labels_buffer_enabled)
    game.set_automap_buffer_enabled(False)
    game.set_mode(settings.game_mode)
    game.set_doom_skill(settings.doom_skill)
    game.set_available_buttons(settings.buttons)
    game.set_screen_format(settings.screen_format)
    game.set_screen_resolution(vzd.ScreenResolution.RES_200X150)
    game.set_available_game_variables([vzd.GameVariable.HEALTH ,vzd.GameVariable.AMMO2])
    game.init()
    print("Doom initialized.")
    return game


def preprocess(img):
    """Down samples image to resolution"""
    img = skimage.transform.resize(img, settings.resolution)
    img = img.astype(np.float32)
    return torch.from_numpy(img)


def create_tensor(game_state):
    """ Creates a ready to prod tensor """
    screen_l = preprocess(game_state.screen_buffer)
    screen_labels = preprocess(game_state.labels_buffer)
    depth_buffer = preprocess(game_state.depth_buffer)
    return torch.stack((screen_l,screen_labels,depth_buffer))


def check_cuda():
    # Uses GPU if available
    if torch.cuda.is_available():
        print("Cuda Available")
    else:
        raise Exception("No cuda")

def create_default_game():
    check_cuda()
    game = create_simple_game()
    return game


def get_s3_client():
    """S3 client"""
    return boto3.client(
        's3',
        aws_access_key_id="AKIAZOWGVPHYIO56WJVS",
        aws_secret_access_key="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"
    )
